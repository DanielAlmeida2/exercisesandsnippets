/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;


import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.io.FileUtils;


/**
 *
 * @author dexho
 */
public class GetDownloadFileSize
{
  public static void main(String[] args)
  {
    try
    {
      File file = new File("C:/clientfull.zip");
      int size = getFileSize(new URL("https://www.dropbox.com/s/1q3tpbngb76v4fr/clientfull.zip?dl=1"));
      long size2 = FileUtils.sizeOf(file);
      System.out.println(size);
      System.out.println(size2);
    }
    catch (MalformedURLException ex)
    {
      System.out.println("ERRO");
    }
  }



  private static int getFileSize(URL url)
  {
    URLConnection conn = null;
    try
    {
        conn = url.openConnection();
        if(conn instanceof HttpURLConnection)
        {
          ((HttpURLConnection)conn).setRequestMethod("HEAD");
        }
        conn.getInputStream();
        return conn.getContentLength();
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
    finally
    {
      if(conn instanceof HttpURLConnection)
      {
        ((HttpURLConnection)conn).disconnect();
      }
    }
}

}

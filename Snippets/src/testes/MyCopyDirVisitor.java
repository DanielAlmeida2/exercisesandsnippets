/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;


import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;


public class MyCopyDirVisitor extends SimpleFileVisitor<Path>
{

  private Path fromPath;
  private Path toPath;
  private StandardCopyOption copyOption;


  public MyCopyDirVisitor (Path fromPath, Path toPath, StandardCopyOption copyOption)
  {
    this.fromPath = fromPath;
    this.toPath = toPath;
    this.copyOption = copyOption;
  }

  public MyCopyDirVisitor (Path fromPath, Path toPath)
  {
    this(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING);
  }

  @Override
  public FileVisitResult preVisitDirectory (Path dir, BasicFileAttributes attrs)throws IOException
  {
    Path targetPath = toPath.resolve(fromPath.relativize(dir));
    if (!Files.exists(targetPath))
    {
      System.out.println("Criando diretório " + targetPath);
      Files.createDirectory(targetPath);
    }
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFile (Path file, BasicFileAttributes attrs)throws IOException
  {
    System.out.println("Copiando arquivo " + file);
    Files.copy(file, toPath.resolve(fromPath.relativize(file)), copyOption);
    return FileVisitResult.CONTINUE;
  }
}

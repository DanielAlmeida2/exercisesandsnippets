/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import org.apache.commons.io.FileUtils;


/**
 *
 * @author dexho
 */
public class EditSpecLine
{
  static short index = 0;
  public static void main(String[] args)
  {

    File file = new File ("C:/Intel/Logs/IntelCPHS.log");
   editSpecificLine(file, "Entrada A: ", "Entrada A: valor 5");



  }



  private static void editSpecificLine(File file, String searchText, String replaceText)
  {
    try
    {
      List<String> readList = FileUtils.readLines(file, "UTF-8");
      int i = 0;
      while (i < readList.size())
      {
        if (readList.get(i).contains(searchText))
        {
          readList.remove(i);
          readList.add(i, replaceText);
        }
        i++;
      }
      System.out.println(readList);
      FileUtils.deleteQuietly(file);
      FileUtils.touch(file);
      i = 0;
      try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file))))
      {
        while (i < readList.size())
        {
          bw.write(readList.get(i));
          if (i+1 != readList.size())
          {
            bw.newLine();
          }
          i++;
        }
      }
    }
    catch (IOException ex)
    {
      System.out.println("error");
    }

  }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;


/**
 *
 * @author dexho
 */
public class FTPApplication
{
  static boolean success2;
  static int percent = 1;
  public static void main(String[] args)
  {
    try
    {
      File file = new File("C:/Teste1/bkp.zip");
      FileUtils.touch(file);
      FTPClient ftp = new FTPClient();
     //ftp.connect("192.168.16.100",1112);
      ftp.connect("71af089e6b3a.sn.mynetname.net",1112);

      ftp.login( "ftpuser", "ftpCode1q3e5t" );
      ftp.enterLocalPassiveMode();
      ftp.changeWorkingDirectory ("/DexCraft/playerbackup");
      ftp.makeDirectory("Teste111");

      Thread uploadInstance = new Thread(() ->
          {
            try
            {
              success2 = uploadSingleFile(ftp, file.toString(), "/DexCraft/playerbackup/Teste111/bkp.zip");
            }
            catch (IOException ex)
            {
              System.out.println("ERRO CRÍTICO EM stepDoBkp() - FALHA AO ENVIAR ARQUIVOS");
            }
          });
          uploadInstance.start();
          monitorUpload(ftp, file);
          percent = 1;
          while (percent <101)
          {
            try
            {
              System.out.println(percent);
              Thread.sleep(1000);
              if (percent == 100) { percent = 101;}
            }
            catch (InterruptedException ex)
            {
              System.out.println("EXCEÇÃO EM UploadProgress THREAD - FALHA NA INTERRUPÇÃO");
            }
          }
          percent = 1;
          uploadSingleFile(ftp, file.toString(), "/DexCraft/playerbackup/Teste111/bkp.zip");



//      String remoteFilename = "bkp.zip";
//      OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(file));
//      boolean success2 = ftp.retrieveFile(remoteFilename, outputStream2);
//      outputStream2.close();
      System.out.println(success2);

//      String[] arq = ftp.listNames();
//      System.out.println ("Listando arquivos: \n");
//
//      for (String f : arq)
//      {
//        System.out.println(f);
//      }
//      Thread.sleep(2000);
//      ftp.makeDirectory("Testeyyy");
//      uploadDirectory(ftp, "/DexCraft/playerbackup/Testeyyy", "C:/DexCraft/bkp", "");
//      System.out.println ("Renomeando backup antigo...\n");
//      ftp.rename("Admin/backup.zip", "Admin/backup.zip.old");
//      System.out.println ("Criando novo Zip...\n");
//      zipFolder("C:/DexCraft/Admin", "C:/DexCraft/Admin/backup.zip");
//      System.out.println ("Fazendo Upload...");
//      uploadSingleFile(ftp ,"C:/DexCraft/Admin/backup.zip", "/DexCraft/playerbackup/Admin/backup.zip");
//      System.out.println ("Removendo Backup antigo...");
//      ftp.deleteFile("Admin/backup.zip.old");
//      System.out.println ("Fazendo upload de credenciais e timestamp...\n");
//      uploadSingleFile(ftp ,"C:/DexCraft/Admin/cred.dc", "/DexCraft/playerbackup/Admin/cred.dc");
      ftp.logout();
      ftp.disconnect();
    }
    catch (IOException ex)
    {
      Logger.getLogger(FTPApplication.class.getName()).log(Level.SEVERE, null, ex);
      System.out.println("ERRO");
    }
//    catch (InterruptedException ex)
//    {
//      Logger.getLogger(FTPApplication.class.getName()).log(Level.SEVERE, null, ex);
//    }
  }


  /**
   * Upload a whole directory (including its nested sub directories and files)
   * to a FTP server.
   *
   * @param ftpClient
   *            an instance of org.apache.commons.net.ftp.FTPClient class.
   * @param remoteDirPath
   *            Path of the destination directory on the server.
   * @param localParentDir
   *            Path of the local directory being uploaded.
   * @param remoteParentDir
   *            Path of the parent directory of the current directory on the
   *            server (used by recursive calls).
   * @throws IOException
   *             if any network or IO error occurred.
   */
  public static void uploadDirectory(FTPClient ftpClient,String remoteDirPath, String localParentDir, String remoteParentDir) throws IOException
  {
    System.out.println("LISTING directory: " + localParentDir);
    File localDir = new File(localParentDir);
    File[] subFiles = localDir.listFiles();
    if (subFiles != null && subFiles.length > 0)
    {
      for (File item : subFiles)
      {
        String remoteFilePath = remoteDirPath + "/" + remoteParentDir + "/" + item.getName();
        if (remoteParentDir.equals(""))
        {
          remoteFilePath = remoteDirPath + "/" + item.getName();
        }
        if (item.isFile())
        {
          // upload the file
          String localFilePath = item.getAbsolutePath();
          System.out.println("About to upload the file: " + localFilePath);
          boolean uploaded = uploadSingleFile(ftpClient,localFilePath, remoteFilePath);
          if (uploaded)
          {
            System.out.println("UPLOADED a file to: " + remoteFilePath);
          }
          else
          {
            System.out.println("COULD NOT upload the file: " + localFilePath);
          }
        }
        else
        {
          // create directory on the server
          boolean created = ftpClient.makeDirectory(remoteFilePath);
          if (created)
          {
            System.out.println("CREATED the directory: " + remoteFilePath);
          }
          else
          {
            System.out.println("COULD NOT create the directory: " + remoteFilePath);
          }
          // upload the sub directory
          String parent = remoteParentDir + "/" + item.getName();
          if (remoteParentDir.equals(""))
          {
            parent = item.getName();
          }
          localParentDir = item.getAbsolutePath();
          uploadDirectory(ftpClient, remoteDirPath, localParentDir, parent);
        }
      }
    }
  }


  /**
   * Upload a single file to the FTP server.
   *
   * @param ftpClient
   *            an instance of org.apache.commons.net.ftp.FTPClient class.
   * @param localFilePath
   *            Path of the file on local computer
   * @param remoteFilePath
   *            Path of the file on remote the server
   * @return true if the file was uploaded successfully, false otherwise
   * @throws IOException
   *             if any network or IO error occurred.
   */
  public static boolean uploadSingleFile(FTPClient ftpClient,String localFilePath, String remoteFilePath) throws IOException
  {
    File localFile = new File(localFilePath);
    try (InputStream inputStream = new FileInputStream(localFile))
    {
      ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
      return ftpClient.storeFile(remoteFilePath, inputStream);
    }
  }


  public static void removeDirectory(FTPClient ftpClient, String parentDir, String currentDir) throws IOException
  {
    String dirToList = parentDir;
    if (!currentDir.equals(""))
    {
      dirToList += "/" + currentDir;
    }
    FTPFile[] subFiles = ftpClient.listFiles(dirToList);
    if (subFiles != null && subFiles.length > 0)
    {
      for (FTPFile aFile : subFiles)
      {
        String currentFileName = aFile.getName();
        if (currentFileName.equals(".") || currentFileName.equals(".."))
        {
          // skip parent directory and the directory itself
          continue;
        }
        String filePath = parentDir + "/" + currentDir + "/" + currentFileName;
        if (currentDir.equals(""))
        {
          filePath = parentDir + "/" + currentFileName;
        }
        if (aFile.isDirectory())
        {
          // remove the sub directory
          removeDirectory(ftpClient, dirToList, currentFileName);
        }
        else
        {
          // delete the file
          boolean deleted = ftpClient.deleteFile(filePath);
          if (deleted)
          {
            System.out.println("DELETED the file: " + filePath);
          }
          else
          {
            System.out.println("CANNOT delete the file: " + filePath);
          }
        }
      }
      // finally, remove the directory itself
      boolean removed = ftpClient.removeDirectory(dirToList);
      if (removed)
      {
        System.out.println("REMOVED the directory: " + dirToList);
      }
      else
      {
        System.out.println("CANNOT remove the directory: " + dirToList);
      }
    }
  }

  static public void monitorUpload(FTPClient ftp, File fil)
  {
    CopyStreamAdapter streamListener = new CopyStreamAdapter()
    {

      @Override
      public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize)
      {
       //this method will be called everytime some bytes are transferred
       percent = (int)(totalBytesTransferred*100/fil.length());
      }
    };
    ftp.setCopyStreamListener(streamListener);
  }





  public static void zipFolder(String sourcePath, String destinationZipPath)
  {
    try
    {
      File directoryToBeZipped = new File(sourcePath);
      File zipFile = new File(destinationZipPath);
      net.lingala.zip4j.core.ZipFile zip = new net.lingala.zip4j.core.ZipFile(zipFile);
      // Adding the list of files and directories to be zipped to a list
      ArrayList<File> fileList = new ArrayList<>();
      Arrays.stream(directoryToBeZipped.listFiles()).forEach((File file) -> {fileList.add(file);});
      ZipParameters parameters = new ZipParameters();
      parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
      parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
      parameters.setEncryptFiles(false);
      fileList.stream().forEach((File f) ->
      {
        try
        {
          if(f.isDirectory())
          {
            zip.addFolder(f, parameters);
          }
          else
          {
            zip.addFile(f, parameters);
          }
        }
        catch(ZipException e)
        {
          System.out.println("ZIP ERROR");
        }
      });
    }
    catch (ZipException ex)
    {
      Logger.getLogger(FTPApplication.class.getName()).log(Level.SEVERE, null, ex);
    }






  }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;


import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.EnumSet;


public class CopyUtil
{

  public static void copyDirectoryContent (File sourceFolder, File destinationFolder) throws IOException
  {
    if (sourceFolder.isDirectory())
    {
      if (destinationFolder.exists() && destinationFolder.isFile())
      {
        throw new IllegalArgumentException ("Destination exists but is not a folder: " + destinationFolder.getAbsolutePath());
      }
      if (!destinationFolder.exists())
      {
        System.out.println("Criando diretório " + destinationFolder.toPath());
        Files.createDirectory(destinationFolder.toPath());
      }
      for (File file : sourceFolder.listFiles())
      {
        if (file.isDirectory())
        {
          System.out.println("Copiando diretório " + destinationFolder.toPath());
          copyDirectory(file, destinationFolder);
        }
        else
        {
          System.out.println("Copiando arquivo " + file);
          copyFile(file, destinationFolder);
        }
      }
    }
  }

  public static void copyDirectory (File fromFile, File toParentFile) throws IOException
  {
    Path from = fromFile.toPath();
    Path to = Paths.get(toParentFile.getAbsolutePath() + File.separatorChar + fromFile.getName());
    Files.walkFileTree(from, EnumSet.of(FileVisitOption.FOLLOW_LINKS),Integer.MAX_VALUE, new MyCopyDirVisitor(from, to));
  }

  public static void copyFile (File toCopy, File mainDestination)throws IOException
  {
    if (!mainDestination.exists())
    {
      mainDestination.mkdirs();
    }
    Path to = Paths.get(mainDestination.getAbsolutePath() +File.separatorChar + toCopy.getName());
    Files.copy(toCopy.toPath(), to, StandardCopyOption.REPLACE_EXISTING);
  }
}
